namespace MyGame.Models
{
    export interface FreeSpins extends RS.Slots.Models.FreeSpins
    {
        triggerWin: number; 
        vaultSpins: number;
        extraSpins: number;
    }

    export namespace FreeSpins
    {
        export function clear(freeSpins: FreeSpins)
        {
            RS.Slots.Models.FreeSpins.clear(freeSpins);
            freeSpins.vaultSpins = null;
            freeSpins.extraSpins = null;
            freeSpins.triggerWin = null;
        }
    }
}