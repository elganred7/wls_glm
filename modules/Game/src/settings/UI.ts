namespace MyGame.Settings
{
    const DesktopButtonPanelSettings: SG.CommonUI.DesktopButtonPanel.Settings =
    {
        ...SG.CommonUI.DesktopButtonPanel.defaultSettings,
        showBigBetButton: true,
    };

    SG.CommonUI.DesktopButtonPanel.defaultSettings = DesktopButtonPanelSettings;

    SG.CommonUI.BottomBar.defaultSettings.gameNameLabel.text = "White Label Slot";
    SG.CommonUI.BottomBar.defaultSettings.showGameName = true;

    SG.CommonUI.DesktopButtonPanel.defaultSettings.gameNameLabel.text = "White Label Slot";
    SG.CommonUI.DesktopButtonPanel.defaultSettings.showGameName = true;
}
