namespace MyGame.Settings
{
    export const BigWin: Red7.BigWin.Controller.Settings =
    {
        viewSettings:
        {
            ...Red7.BigWin.View.defaultSettings,
            dimensions: { w: 1736, h: 768 }
        },
        volume: 1.0,
        dimVolume: 0.0,
        thresholds:
        {
            big: 20,
            super: 50,
            mega: 75,
            epic: 100
        }
    };
}